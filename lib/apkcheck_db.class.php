<?php
require_once(__DIR__.'/sqlite3.class.php');

/** Database interface for our apk checker
 * @class apkcheck_db
 * @author Izzy (izzysoft AT qumran DOT org)
 * @copyright Andreas Itzchak Rehberg (GPLv2)
 * @verbatim
 *  Example flow (incomplete):
 *  $db = new apkcheck_db('apkcheck.db');
 *  // add a new file to the queue
 *  if ( $db->add2q('com.foobar','com.foobar_1.0.apk') ) echo "Success.\n";
 *  else echo "Failed!\n";
 *  // update after having submitted it to VT (-2: enqueued)
 *  $db->updateQ('com.foobar_1.0.apk','','0123456789ABCDEF',-2);
 *  print_r($db->getQ());
 *  // update results once retrieved
 *  $db->updateFile('com.foobar_1.0.apk','','com.foobar','','',0,'','');
 *  print_r($db->getFile('com.foobar_1.0.apk'));
 *  print_r($t->listFileNames());
 *  // some very later day, we removed this APK from the repo, so:
 *  $db->deleteFile('com.foobar_1.0.apk');
 */
class apkcheck_db extends db_sqlite {

  /** Initialize the class by setting up its database file
   * @constructor apkcheck_db
   * @param string dbfile SQLite database file to use (must exist and be accessible of course)
   */
  public function __construct($dbfile) {
    $this->Database = $dbfile;
    $this->Halt_On_Error = "report";
  }

  // -----------------------------------------------------=[ Queue handling ]=--
  /** Enqueue a file for check by VirusTotal
   * @class apkcheck_db
   * @method add2q
   * @param str pkgname package name of the corresponding app
   * @param str file name of the file to enqueue
   * @param opt str url corresponding (Github) URL
   * @return bool success
   */
  public function add2q($pkgname,$file,$url='') {
    $res = $this->query_single_row("SELECT COUNT(*) AS apps FROM job_queue WHERE local_file_name='".$file."'");
    if ( $res['apps'] != 0 ) {
      // trigger_error("File '$file' is already queued.",E_USER_NOTICE);
      return TRUE;
    }
    $this->query('BEGIN TRANSACTION');
    $res = $this->query_single_row("SELECT COUNT(*) AS apps FROM files WHERE local_file_name='".$file."'");
    if ( $res['apps'] == 0 ) {
      if ( empty($url) ) $url = 'NULL';
      else $url = "'".$this->escape($url)."'";
      if ( !$this->query("INSERT INTO files (local_file_name,github_file_url,pkgname) VALUES ('$file',$url,'$pkgname')") ) goto Fail;
    }
    if ( !$this->query("INSERT INTO job_queue (local_file_name,entry_time) VALUES ('$file',strftime('%s','now'))") ) goto Fail;
    Success:
      $this->query('COMMIT TRANSACTION');
      return TRUE;
    Fail:
      $this->query('ROLLBACK');
      return FALSE;
  }

  /** Get queued files
   * @class apkcheck_db
   * @method getQ
   * @param opt str qtype Type of enqueued files: ALL (default), IDLE (just pushed but not yet processed), WAITING (uploaded, no result yet), READY (have results)
   * @return array queueItems 0..n of [local_file_name,vt_json,vt_hash,vt_response_code,entry_time]
   */
  public function getQ($qtype='ALL') {
    $qtype = strtoupper($qtype);
    if ( !in_array($qtype,['ALL','IDLE','WAITING','READY']) ) $qtype = 'ALL';
    switch($qtype) {
      case 'IDLE':
        $where = "WHERE vt_response_code IS NULL or vt_response_code NOT IN (1,-2)";
        break;
      case 'WAITING':
        $where = "WHERE vt_response_code=-2";
        break;
      case 'READY':
        $where = "WHERE vt_response_code=1";
        break;
      default:
        $where = '';
        break;
    }
    $query = "SELECT local_file_name,vt_json,vt_hash,vt_response_code,entry_time FROM job_queue $where ORDER BY entry_time ASC";
    $this->query($query);
    $res = [];
    while ( $this->next_record() ) {
      $res[] = ['local_file_name'=>$this->f('local_file_name'),'vt_json'=>$this->f('vt_json'),'vt_hash'=>$this->f('vt_hash'),'vt_response_code'=>$this->f('vt_response_code'),'entry_time'=>$this->f('entry_time')];
    }
    return $res;
  }

  /** Update queue status for a file
   * @class apkcheck_db
   * @method updateQ
   * @param str file    name of the file
   * @param str vt_json VirusTotal JSON
   * @param str vt_hash VirusTotal hash
   * @param int vt_response_code Response code given by VirusTotal
   * @return bool success
   */
  public function updateQ($file,$vt_json,$vt_hash,$vt_response_code) {
    ( empty($vt_json) ) ? $vt_json = 'NULL' : $vt_json = "'".$this->escape($vt_json)."'";
    ( empty($vt_hash) ) ? $vt_hash = 'NULL' : $vt_hash = "'$vt_hash'";
    if ( empty($vt_response_code) ) $vt_response_code = 'NULL';
    $query = "UPDATE job_queue SET vt_json=$vt_json,vt_hash=$vt_hash,vt_response_code=$vt_response_code WHERE local_file_name='$file'";
    if ( $this->query($query) ) return TRUE;
    return FALSE;
  }

  /** Remove an entry from the Q (usually once it was successfully processed)
   * @class apkcheck_db
   * @method delFromQ
   * @param str file    name of the file
   * @return bool success
   */
  function delFromQ($file) {
    return $this->query("DELETE FROM job_queue WHERE local_file_name='$file'");
  }

  // ------------------------------------------------------=[ File handling ]=--
  /** List all file names we have recorded
   * @class apkcheck_db
   * @method listFileNames
   * @param opt bool infectedOnly=FALSE
   * @return array of str fileNames
   */
  public function listFileNames($infectedOnly=FALSE) {
    if ( $infectedOnly ) $where = "WHERE vt_detected > 0";
    else $where = '';
    $this->query("SELECT local_file_name FROM files $where");
    $res = [];
    while ( $this->next_record() ) $res[] = $this->f('local_file_name');
    return $res;
  }

  /** Get the record for a file
   * @class apkcheck_db
   * @method getFile
   * @param str file    name of the file
   * @return array      fileInfo: strings local_file_name, github_file_url, pkgname, vt_permalink; JSON vt_result, apk_libraries, apk_paymodules, apk_admodules, apk_analyticsmodules; ints vt_detected, libcount
   */
  public function getFile($file) {
    $query = "SELECT github_file_url,pkgname,vt_permalink,vt_result,vt_detected,apk_libraries,apk_paymodules,apk_admodules,apk_analyticsmodules,libcount FROM files WHERE local_file_name='$file'";
    if ( $this->query($query) && $this->next_record() ) {
      return [
        'local_file_name'=>$file,
        'github_file_url'=>$this->f('github_file_url'),
        'pkgname'=>$this->f('pkgname'),
        'vt_permalink'=>$this->f('vt_permalink'),
        'vt_result'=>$this->f('vt_result'),
        'vt_detected'=>$this->f('vt_detected'),
        'apk_libraries'=>$this->f('apk_libraries'),
        'apk_paymodules'=>$this->f('apk_paymodules'),
        'apk_admodules'=>$this->f('apk_admodules'),
        'apk_analyticsmodules'=>$this->f('apk_analyticsmodules'),
        'libcount'=>$this->f('libcount')
      ];
    } else {
      return [];
    }
  }

  /** Update a file record
   * @class apkcheck_db
   * @method updateFile
   * @param str file            name of the file
   * @param str github_file_url corresponding URL
   * @param str pkgname         corresponding package name
   * @param str vt_permalink    VirusTotal permalink
   * @param str vt_result       VirusTotal JSON result set
   * @param int vt_detected     number of detected threats
   * @param str apk_libraries   JSON with details on other libraries detected
   * @param str apk_paymodules  JSON with details on payment modules detected
   * @param str apk_admodules   JSON with details on ad modules detected
   * @param str apk_analyticsmodules JSON with details on analytics modules detected
   * @param int libcount        number of libraries altogether
   * @return bool success
   */
  public function updateFile($file,$github_file_url,$pkgname,$vt_permalink,$vt_result,$vt_detected,$apk_libraries,$apk_paymodules,$apk_admodules,$apk_analyticsmodules,$libcount) {
    foreach ( ['vt_result','apk_libraries','apk_paymodules','apk_admodules','apk_analyticsmodules'] as $item ) {
      ( empty(${$item}) ) ? ${$item} = 'NULL' : ${$item} = "'".$this->escape(${$item})."'";
    }
    foreach ( ['github_file_url','vt_permalink'] as $item ) {
      ( empty(${$item}) ) ? ${$item} = 'NULL' : ${$item} = "'".${$item}."'";
    }
    if ( empty($vt_detected) && $vt_detected!==0 ) $vt_detected = 'NULL';
    if ( empty($libcount) ) $libcount = 0;
    $query = "UPDATE files
                 SET github_file_url=$github_file_url,pkgname='$pkgname',vt_permalink=$vt_permalink,vt_result=$vt_result,vt_detected=$vt_detected,apk_libraries=$apk_libraries,apk_paymodules=$apk_paymodules,apk_admodules=$apk_admodules,apk_analyticsmodules=$apk_analyticsmodules,libcount=$libcount
               WHERE local_file_name='$file'";
    if ( $this->query($query) ) return TRUE;
    else return FALSE;
  }

  /** Delete a file from our records
   * @class apkcheck_db
   * @method updateFile
   * @param str file            name of the file
   * @return bool success
   */
  public function deleteFile($file) {
    return $this->query("DELETE FROM files WHERE local_file_name='$file'");
  }

  // --------------------------------------------------------=[ Maintenance ]=--
  /** Compress database
   * @class apkcheck_db
   * @method compress
   */
  function compress() {
    $this->query('VACUUM');
  }

}
?>